## tasks


## 1 : Python program on local Jupyter installation

Source code and the generated files are in task1 folder.

screenshots :
![alt text](task1/output.png)

---

## 2 : Weather JupyterLab extension

Extension published on npm : @meyash/labweather
Link : https://www.npmjs.com/package/@meyash/labweather  

Installation Instructions :
1) conda create -n labweather jupyterlab nodejs
2) conda activate labweather
3) jupyter labextension install @meyash/labweather
4) jupyter lab

I tried 3 ways of running extensions :
1) via launcher icon
2) via left side bar
3) via command palette

In my published extension I have used the 1st way i.e accessing via launcher icon.

screenshots : 
![alt text](task2/leftpanel.png)
![alt text](task2/tab.png)
![alt text](task2/launchericon.png)

---

## 3 : Make the weather accessible inside the notebook (optional)

Extension published on pypi : https://test.pypi.org/project/swanweather/

Install Instuctions :
1) python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps swanweather

Using inside notebook :
1) load module 
    %load_ext swanweather

2) get weather 
    json directly %weather city_name

3) get weather and store in variable 
    data=weather('city_name')

screenshots : 
![alt text](task3/one.png)
![alt text](task3/two.png)